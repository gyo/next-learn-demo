import Layout from "../components/MyLayout.js";
import Link from "next/link";
import fetch from "isomorphic-unfetch";

const ShowLink = ({ show }) => (
  <React.Fragment key={show.id}>
    <li>
      <Link as={`/p/${show.id}`} href={`/post?id=${show.id}`}>
        <a>{show.name}</a>
      </Link>
    </li>
    <style jsx>{`
      li {
        list-style: none;
        margin: 5px 0;
      }

      a {
        font-family: "Arial";
        text-decoration: none;
        color: blue;
      }

      a:hover {
        opacity: 0.6;
      }
    `}</style>
  </React.Fragment>
);

function Index(props) {
  return (
    <>
      <Layout>
        <h1>Batman TV Shows</h1>
        <ul>{props.shows.map(({ show }) => ShowLink({ show }))}</ul>
      </Layout>
      <style jsx>{`
        h1 {
          font-family: "Arial";
        }

        ul {
          padding: 0;
        }
      `}</style>
    </>
  );
}

Index.getInitialProps = async function() {
  const res = await fetch("http://api.tvmaze.com/search/shows?q=batman");
  const data = await res.json();

  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    shows: data
  };
};

export default Index;
